def caesar_cipher(pre_ciphered_string, num_of_shifts)
  ciphered_string = Array.new # Create new string to hold the ciphered string
  pre_ciphered_string.each_char do |char| # Loop over the string character by character and check if its in the alphabet
    if char.ord >= 65 && char.ord <= 90
      (char.ord + num_of_shifts) > 90 ? ciphered_string.push((char.ord + num_of_shifts) - 26) : ciphered_string.push(char.ord + num_of_shifts)
    elsif char.ord >= 97 && char.ord <= 122
      (char.ord + num_of_shifts) > 122 ? ciphered_string.push((char.ord + num_of_shifts) - 26) : ciphered_string.push(char.ord + num_of_shifts)
    else
      ciphered_string.push(char) # If the charcater is not in the alphabet{spaces,!, &...etc} added it to the array without modifying it
    end
  end
  puts ciphered_string.map{|byte| byte.chr}.join('') # Convert the ascii value back to characters and display them
end

print "Please enter the string to cipher: "
pre_ciphered = gets.chomp

print "Enter how many characters to shift by: "
shift_amount = gets.chomp.to_i

caesar_cipher(pre_ciphered,shift_amount)